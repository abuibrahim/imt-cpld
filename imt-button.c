// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2018 Ruslan Babayev.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>

static irqreturn_t imt_button_isr(int irq, void *data)
{
	struct input_dev *idev = data;

	input_report_key(idev, KEY_POWER, 1);
	input_sync(idev);

	return IRQ_HANDLED;
}

static int imt_button_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct input_dev *idev;
	int irq, err;

	irq = platform_get_irq(pdev, 0);
	if (irq < 0)
		return irq;

	idev = devm_input_allocate_device(dev);
	if (!idev) {
		dev_err(dev, "failed allocating input device");
		return -ENOMEM;
	}
	input_set_capability(idev, EV_KEY, KEY_POWER);

	idev->name = "IMT Button";
	idev->phys = "imt/input0";
	platform_set_drvdata(pdev, idev);

	err = devm_request_threaded_irq(dev, irq, NULL, imt_button_isr,
					IRQF_ONESHOT, "imt-button", idev);
	if (err) {
		dev_err(dev, "failed request for irq %d", irq);
		return err;
	}

	return input_register_device(idev);
}

static int imt_button_remove(struct platform_device *pdev)
{
	struct input_dev *input = platform_get_drvdata(pdev);

	input_unregister_device(input);

	return 0;
}

static struct of_device_id imt_button_device_ids[] = {
	{ .compatible = "imt,button", },
	{},
};
MODULE_DEVICE_TABLE(of, imt_button_device_ids);

static struct platform_driver imt_button_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "imt-button",
		.of_match_table = imt_button_device_ids,
	},
	.probe		= imt_button_probe,
	.remove		= imt_button_remove,
};
module_platform_driver(imt_button_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@babayev.com>");
MODULE_DESCRIPTION("IMT button driver");
MODULE_LICENSE("GPL");
