ifneq ($(KERNELRELEASE),)
obj-m += imt-intc.o
obj-m += imt-mux.o
obj-m += imt-i2c.o
obj-m += imt-i2c-bitbang.o
obj-m += imt-button.o
obj-m += imt-poweroff.o
else
all:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD modules

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD modules_install

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD clean
endif
