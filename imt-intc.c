// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2018 Ruslan Babayev.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>

#define NUM_IRQS	8

struct imt_intc {
	struct irq_domain *domain;
	struct mutex lock;
	void __iomem *base;
	int irq;
	u8 mask;
};

static void imt_irq_lock(struct irq_data *d)
{
	struct imt_intc *data = irq_data_get_irq_chip_data(d);

	mutex_lock(&data->lock);
}

static void imt_irq_sync_unlock(struct irq_data *d)
{
	struct imt_intc *data = irq_data_get_irq_chip_data(d);

	iowrite8(~data->mask, data->base + 1);
	mutex_unlock(&data->lock);
}

static void imt_irq_enable(struct irq_data *d)
{
	struct imt_intc *data = irq_data_get_irq_chip_data(d);

	data->mask &= ~BIT(d->hwirq);
}

static void imt_irq_disable(struct irq_data *d)
{
	struct imt_intc *data = irq_data_get_irq_chip_data(d);

	data->mask |= BIT(d->hwirq);
}

static struct irq_chip imt_irq_chip = {
	.name			= "IMT",
	.irq_bus_lock		= imt_irq_lock,
	.irq_bus_sync_unlock	= imt_irq_sync_unlock,
	.irq_enable		= imt_irq_enable,
	.irq_disable		= imt_irq_disable,
};

static int imt_irq_map(struct irq_domain *d, unsigned int irq,
		       irq_hw_number_t hw)
{
	struct imt_intc *data = d->host_data;

	irq_set_chip_data(irq, data);
	irq_set_chip(irq, &imt_irq_chip);
	irq_set_nested_thread(irq, true);
	irq_set_parent(irq, data->irq);
	irq_set_noprobe(irq);

	return 0;
}

static const struct irq_domain_ops imt_domain_ops = {
	.map	= imt_irq_map,
	.xlate	= irq_domain_xlate_onetwocell,
};

static irqreturn_t imt_irq_thread(int irq, void *d)
{
	struct imt_intc *data = d;
	irqreturn_t ret = IRQ_NONE;
	irq_hw_number_t hwirq;
	u8 stat;

	stat = ioread8(data->base);
	stat &= ~data->mask;		/* ignore masked */
	iowrite8(stat, data->base);	/* ack */

	for (hwirq = 0; hwirq < NUM_IRQS; hwirq++) {
		if (stat & BIT(hwirq)) {
			handle_nested_irq(irq_find_mapping(data->domain, hwirq));
			ret = IRQ_HANDLED;
		}
	}

	return ret;
}

static int imt_intc_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct resource *res;
	struct imt_intc *data;
	int err;

	data = devm_kzalloc(dev, sizeof (*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	platform_set_drvdata(pdev, data);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
		return -ENOENT;

	data->irq = platform_get_irq(pdev, 0);
	if (data->irq < 0)
		return data->irq;

	data->base = devm_ioremap(dev, res->start, resource_size(res));
	if (!data->base)
		return PTR_ERR(data->base);

	mutex_init(&data->lock);
	data->mask = 0xff; /* mask all */
	iowrite8(~data->mask, data->base + 1);

	data->domain = irq_domain_add_linear(np, NUM_IRQS, &imt_domain_ops,
					     data);
	if (!data->domain)
		return PTR_ERR(data->domain);

	err = request_threaded_irq(data->irq, NULL, imt_irq_thread,
				   IRQF_SHARED | IRQF_ONESHOT,
				   imt_irq_chip.name, data);
	if (err) {
		irq_domain_remove(data->domain);
		return err;
	}

	return 0;
}

static int imt_intc_remove(struct platform_device *pdev)
{
	struct imt_intc *data = platform_get_drvdata(pdev);
	irq_hw_number_t hwirq;
	unsigned int virq;

	free_irq(data->irq, data);
	for (hwirq = 0; hwirq < NUM_IRQS; hwirq++) {
		virq = irq_find_mapping(data->domain, hwirq);
		if (virq)
			irq_dispose_mapping(virq);
	}
	irq_domain_remove(data->domain);

	return 0;
}


static struct of_device_id imt_intc_device_ids[] = {
	{ .compatible = "imt,intc" },
	{},
};
MODULE_DEVICE_TABLE(of, imt_intc_device_ids);

static struct platform_driver imt_intc_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "imt-intc",
		.of_match_table = imt_intc_device_ids,
	},
	.probe		= imt_intc_probe,
	.remove		= imt_intc_remove,
};
module_platform_driver(imt_intc_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@viptela.com>");
MODULE_DESCRIPTION("IMT Interrupt Controller driver");
MODULE_LICENSE("GPL");
