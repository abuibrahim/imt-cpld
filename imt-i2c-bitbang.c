/*
 * Copyright 2014, Viptela Inc.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/i2c.h>
#include <linux/i2c-algo-bit.h>
#include <linux/reset-controller.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>

struct imt_i2c_bitbang {
	struct i2c_adapter adap;
	struct i2c_algo_bit_data algo_bit;
	struct reset_controller_dev rcdev;
	void __iomem *reg;
};

static void imt_i2c_bitbang_setsda(void *data, int level)
{
	struct imt_i2c_bitbang *i2c = data;
	u8 val;

	val = ioread8(i2c->reg);

	if (level)
		val |= 8;
	else
		val &= ~8;

	iowrite8(val, i2c->reg);
}

static void imt_i2c_bitbang_setscl(void *data, int level)
{
	struct imt_i2c_bitbang *i2c = data;
	u8 val;

	val = ioread8(i2c->reg);

	if (level)
		val |= 4;
	else
		val &= ~4;

	iowrite8(val, i2c->reg);
}

static int imt_i2c_bitbang_getsda(void *data)
{
	struct imt_i2c_bitbang *i2c = data;
	u8 val;

	val = ioread8(i2c->reg);

	return !!(val & 2);
}

static int imt_i2c_bitbang_getscl(void *data)
{
	struct imt_i2c_bitbang *i2c = data;
	u8 val;

	val = ioread8(i2c->reg);

	return !!(val & 1);
}

static int imt_reset_assert(struct reset_controller_dev *rcdev,
			    unsigned long id)
{
	struct imt_i2c_bitbang *i2c = dev_get_drvdata(rcdev->dev);
	u8 val;

	val = ioread8(i2c->reg);
	iowrite8(val | 0x10, i2c->reg);

	return 0;
}

static int imt_reset_deassert(struct reset_controller_dev *rcdev,
			      unsigned long id)
{
	struct imt_i2c_bitbang *i2c = dev_get_drvdata(rcdev->dev);
	u8 val;

	val = ioread8(i2c->reg);
	iowrite8(val & ~0x10, i2c->reg);

	return 0;
}

static int imt_reset_status(struct reset_controller_dev *rcdev,
			    unsigned long id)
{
	struct imt_i2c_bitbang *i2c = dev_get_drvdata(rcdev->dev);
	u8 val;

	val = ioread8(i2c->reg);
	return !!(val & 0x10);
}

static int imt_reset_of_xlate(struct reset_controller_dev *rcdev,
			      const struct of_phandle_args *reset_spec)
{
	if (WARN_ON(reset_spec->args_count != 0))
		return -EINVAL;

	return 0;
}

static const struct reset_control_ops imt_reset_ops = {
	.assert		= imt_reset_assert,
	.deassert	= imt_reset_deassert,
	.status		= imt_reset_status,
};

static int imt_i2c_bitbang_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct imt_i2c_bitbang *i2c;
	struct resource *res;
	int err;

	i2c = devm_kzalloc(dev, sizeof (*i2c), GFP_KERNEL);
	if (!i2c)
		return -ENOMEM;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(&pdev->dev, "found no memory resource\n");
		return -ENODEV;
	}

	i2c->reg = devm_ioremap_resource(dev, res);
	if (IS_ERR(i2c->reg)) {
		dev_err(dev, "could not map device register\n");
		return PTR_ERR(i2c->reg);
	}

	platform_set_drvdata(pdev, i2c);

	i2c->rcdev.owner = THIS_MODULE;
	i2c->rcdev.dev = dev;
	i2c->rcdev.nr_resets = 1;
	i2c->rcdev.ops = &imt_reset_ops;
	i2c->rcdev.of_node = np;
	i2c->rcdev.of_reset_n_cells = 0;
	i2c->rcdev.of_xlate = imt_reset_of_xlate;

	err = devm_reset_controller_register(dev, &i2c->rcdev);
	if (err)
		return err;

	i2c_set_adapdata(&i2c->adap, i2c);
	i2c->adap.owner = THIS_MODULE;
	i2c->adap.class = I2C_CLASS_HWMON;
	i2c->adap.dev.parent = dev;
	i2c->adap.dev.of_node = np;
	i2c->adap.algo_data = &i2c->algo_bit;
	i2c->algo_bit.data = i2c;
	i2c->algo_bit.setsda = imt_i2c_bitbang_setsda;
	i2c->algo_bit.setscl = imt_i2c_bitbang_setscl;
	i2c->algo_bit.getsda = imt_i2c_bitbang_getsda;
	i2c->algo_bit.getscl = imt_i2c_bitbang_getscl;
	i2c->algo_bit.timeout = msecs_to_jiffies(10);
	i2c->algo_bit.udelay = 10;
	strlcpy(i2c->adap.name, "IMT bitbang adapter", sizeof (i2c->adap.name));

	return i2c_bit_add_bus(&i2c->adap);
}

static int imt_i2c_bitbang_remove(struct platform_device *pdev)
{
	struct imt_i2c_bitbang *i2c = platform_get_drvdata(pdev);

	i2c_del_adapter(&i2c->adap);

	return 0;
}

static struct of_device_id imt_i2c_bitbang_device_ids[] = {
	{ .compatible = "imt,i2c-bitbang", },
	{},
};
MODULE_DEVICE_TABLE(of, imt_i2c_bitbang_device_ids);

static struct platform_driver imt_i2c_bitbang_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "i2c-bb-imt",
		.of_match_table = imt_i2c_bitbang_device_ids,
	},
	.probe		= imt_i2c_bitbang_probe,
	.remove		= imt_i2c_bitbang_remove,
};
module_platform_driver(imt_i2c_bitbang_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@babayev.com>");
MODULE_DESCRIPTION("IMT I2C bitbang driver");
MODULE_LICENSE("GPL");
