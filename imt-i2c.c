// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2018 Ruslan Babayev.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/completion.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>

#define IMT_I2C_CMD			0
#define IMT_I2C_STAT			1
#define  IMT_I2C_STAT_FLT		BIT(5)
#define  IMT_I2C_STAT_TIMEOUT		BIT(4)
#define  IMT_I2C_STAT_NOACK		BIT(3)
#define  IMT_I2C_STAT_CSINV		BIT(2)
#define  IMT_I2C_STAT_BUSY		BIT(0)
#define  IMT_I2C_STAT_RESET		BIT(0)
#define IMT_I2C_MCTL			2
#define IMT_I2C_DEVID0			3
#define IMT_I2C_DEVID1			4
#define IMT_I2C_DB1			5
#define IMT_I2C_DB2			6
#define IMT_I2C_DB3			7

#define IMT_I2C_SEQ0_NOP		0
#define IMT_I2C_SEQ0_TX_DEV1		1
#define IMT_I2C_SEQ0_TX_DEV0_INV	2
#define IMT_I2C_SEQ0_RES		3

#define IMT_I2C_SEQ1_NOP		(0 << 2)
#define IMT_I2C_SEQ1_TX_DB1		(1 << 2)
#define IMT_I2C_SEQ1_RX_DB1_ACK		(3 << 2)

#define IMT_I2C_SEQ2_NOP		(0 << 4)
#define IMT_I2C_SEQ2_TX_DB2		(1 << 4)
#define IMT_I2C_SEQ2_RX_DB2_ACK		(3 << 4)

#define IMT_I2C_SEQ3_NOP		(0 << 6)
#define IMT_I2C_SEQ3_TX_DB3_STOP	(1 << 6)
#define IMT_I2C_SEQ3_STOP_RST		(2 << 6)
#define IMT_I2C_SEQ3_RX_DB3_NACK_STOP	(3 << 6)

#define IMT_I2C_SEQ_BUS_RESET				\
	(IMT_I2C_SEQ0_NOP | IMT_I2C_SEQ1_NOP |		\
	 IMT_I2C_SEQ2_NOP | IMT_I2C_SEQ3_STOP_RST)

struct imt_i2c {
	struct i2c_adapter adap;
	struct completion done;
	void __iomem *regs;
	int irq;
};

static irqreturn_t imt_i2c_isr(int irq, void *priv)
{
	struct imt_i2c *i2c = priv;

	disable_irq_nosync(i2c->irq);
	complete(&i2c->done);

	return IRQ_HANDLED;
}

static int imt_i2c_wait(struct i2c_adapter *adap)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	u8 stat;

	reinit_completion(&i2c->done);
	enable_irq(i2c->irq);
	if (!wait_for_completion_timeout(&i2c->done, adap->timeout))
		disable_irq_nosync(i2c->irq);

	stat = ioread8(i2c->regs + IMT_I2C_STAT);
	if (stat & IMT_I2C_STAT_FLT) {
		dev_warn(&adap->dev, "fault detected, trying bus recovery");
		i2c_recover_bus(adap);
		return -EAGAIN;
	} else if (stat & IMT_I2C_STAT_TIMEOUT) {
		return -ETIMEDOUT;
	} else if (stat & IMT_I2C_STAT_NOACK) {
		return -ETIMEDOUT;
	} else if (!(stat & IMT_I2C_STAT_BUSY)) {
		return 0;
	} else {
		return -ETIMEDOUT;
	}
}

static int imt_i2c_read(struct i2c_adapter *adap, struct i2c_msg *msg)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	int err;
	u16 len = msg->len;
	u8 *buf = msg->buf;
	u8 seq, devid1;

	devid1 = (msg->addr << 1) | 1;
	iowrite8(devid1, i2c->regs + IMT_I2C_DEVID1);

	seq = IMT_I2C_SEQ0_TX_DEV1;
	if (len == 0) {
		seq |= IMT_I2C_SEQ3_STOP_RST;
		iowrite8(seq, i2c->regs + IMT_I2C_CMD);
		err = imt_i2c_wait(adap);
		if (err)
			return err;
	}

	while (len) {
		switch (len) {
		case 3:	seq |= IMT_I2C_SEQ1_RX_DB1_ACK; fallthrough;
		case 2:	seq |= IMT_I2C_SEQ2_RX_DB2_ACK; fallthrough;
		case 1:	seq |= IMT_I2C_SEQ3_RX_DB3_NACK_STOP;
			break;
		default:
			seq |= IMT_I2C_SEQ1_RX_DB1_ACK;
			seq |= IMT_I2C_SEQ2_RX_DB2_ACK;
		}

		iowrite8(seq, i2c->regs + IMT_I2C_CMD);
		err = imt_i2c_wait(adap);
		if (err)
			return err;

		seq = 0;
		switch (len) {
		case 3:	*buf++ = ioread8(i2c->regs + IMT_I2C_DB1); fallthrough;
		case 2:	*buf++ = ioread8(i2c->regs + IMT_I2C_DB2); fallthrough;
		case 1:	*buf++ = ioread8(i2c->regs + IMT_I2C_DB3);
			len = 0;
			break;
		default:
			*buf++ = ioread8(i2c->regs + IMT_I2C_DB1);
			*buf++ = ioread8(i2c->regs + IMT_I2C_DB2);
			len -= 2;
		}
	}

	return 0;
}

static int imt_i2c_write(struct i2c_adapter *adap, struct i2c_msg *msg)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	int err;
	u16 len = msg->len;
	u8 *buf = msg->buf;
	u8 seq, devid1;

	devid1 = msg->addr << 1;
	iowrite8(devid1, i2c->regs + IMT_I2C_DEVID1);
	if (len == 0) {
		seq = IMT_I2C_SEQ0_TX_DEV1 | IMT_I2C_SEQ3_STOP_RST;
		iowrite8(seq, i2c->regs + IMT_I2C_CMD);
		err = imt_i2c_wait(adap);
		if (err)
			return err;
	}

	seq = IMT_I2C_SEQ0_TX_DEV1;
	while (len) {
		switch (len) {
		case 3:	seq |= IMT_I2C_SEQ1_TX_DB1;
			iowrite8(*buf++, i2c->regs + IMT_I2C_DB1);
			fallthrough;
		case 2:	seq |= IMT_I2C_SEQ2_TX_DB2;
			iowrite8(*buf++, i2c->regs + IMT_I2C_DB2);
			fallthrough;
		case 1:	seq |= IMT_I2C_SEQ3_TX_DB3_STOP;
			iowrite8(*buf++, i2c->regs + IMT_I2C_DB3);
			len = 0;
			break;
		default:
			seq |= IMT_I2C_SEQ1_TX_DB1;
			seq |= IMT_I2C_SEQ2_TX_DB2;
			seq |= IMT_I2C_SEQ3_NOP;
			iowrite8(*buf++, i2c->regs + IMT_I2C_DB1);
			iowrite8(*buf++, i2c->regs + IMT_I2C_DB2);
			len -= 2;
			break;
		}

		iowrite8(seq, i2c->regs + IMT_I2C_CMD);
		err = imt_i2c_wait(adap);
		if (err)
			return err;
		seq = 0;
	}

	return 0;
}

static int imt_i2c_master_xfer(struct i2c_adapter *adap, struct i2c_msg *msgs,
			       int num)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	int i, err;
	u8 stat;

	stat = ioread8(i2c->regs + IMT_I2C_STAT);
	stat |= IMT_I2C_STAT_RESET;
	iowrite8(stat, i2c->regs + IMT_I2C_STAT);

	for (i = 0; i < num; i++) {
		if (msgs[i].flags & I2C_M_RD) {
			err = imt_i2c_read(adap, &msgs[i]);
			if (err)
				return err;
		} else {
			err = imt_i2c_write(adap, &msgs[i]);
			if (err)
				return err;
		}
	}

	return num;
}

static u32 imt_i2c_functionality(struct i2c_adapter *adap)
{
	return I2C_FUNC_I2C | I2C_FUNC_SMBUS_EMUL;
}

static const struct i2c_algorithm imt_i2c_algo = {
	.master_xfer	= imt_i2c_master_xfer,
	.functionality	= imt_i2c_functionality,
};

static int imt_i2c_get_scl(struct i2c_adapter *adap)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	u8 val;

	val = ioread8(i2c->regs + IMT_I2C_MCTL);

	return !!(val & 1);
}

static void imt_i2c_set_scl(struct i2c_adapter *adap, int level)
{
	struct imt_i2c *i2c = i2c_get_adapdata(adap);
	u8 val;

	val = ioread8(i2c->regs + IMT_I2C_MCTL);

	if (level)
		val |= 4;
	else
		val &= ~4;

	iowrite8(val, i2c->regs + IMT_I2C_MCTL);
}

static struct i2c_bus_recovery_info imt_i2c_bus_recovery_info = {
	.recover_bus	= i2c_generic_scl_recovery,
	.get_scl	= imt_i2c_get_scl,
	.set_scl	= imt_i2c_set_scl,
};

static int imt_i2c_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct imt_i2c *i2c;
	struct resource *res;
	int ret;
	u32 freq;
	u8 rate;

	i2c = devm_kzalloc(dev, sizeof (*i2c), GFP_KERNEL);
	if (!i2c)
		return -ENOMEM;

	platform_set_drvdata(pdev, i2c);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
		return -EINVAL;

	i2c->regs = devm_ioremap_resource(dev, res);
	if (IS_ERR(i2c->regs))
		return PTR_ERR(i2c->regs);

	i2c->irq = platform_get_irq(pdev, 0);
	if (i2c->irq < 0)
		return i2c->irq;

	ret = of_property_read_u32(np, "clock-frequency", &freq);
	if (ret < 0) {
		dev_warn(dev, "could not read clock-frequency");
	        freq= 100000;
	}

	switch (freq) {
	case 25000:
		rate = 3;
		break;
	case 50000:
		rate = 0;
		break;
	case 100000:
		rate = 1;
		break;
	case 400000:
		rate = 2;
		break;
	default:
		dev_err(dev, "unsupported clock-frequency %d", freq);
		return -EINVAL;
	}

	init_completion(&i2c->done);
	i2c->adap.owner = THIS_MODULE;
	i2c->adap.class = I2C_CLASS_HWMON;
	i2c->adap.dev.parent = dev;
	i2c->adap.dev.of_node = np;
	i2c->adap.algo = &imt_i2c_algo;
	i2c->adap.timeout = msecs_to_jiffies(100);
	i2c->adap.bus_recovery_info = &imt_i2c_bus_recovery_info;
	strlcpy(i2c->adap.name, "IMT adapter", sizeof (i2c->adap.name));
	i2c_set_adapdata(&i2c->adap, i2c);

	iowrite8(IMT_I2C_STAT_RESET, i2c->regs + IMT_I2C_STAT);
	iowrite8(rate << 6, i2c->regs + IMT_I2C_MCTL);
	iowrite8(IMT_I2C_SEQ_BUS_RESET, i2c->regs + IMT_I2C_CMD);

	ret = devm_request_threaded_irq(dev, i2c->irq, NULL, imt_i2c_isr, 0,
					"imt-i2c", i2c);
	if (ret) {
		dev_err(dev, "failed request for irq %d", i2c->irq);
		return ret;
	}

	return i2c_add_adapter(&i2c->adap);
}

static int imt_i2c_remove(struct platform_device *pdev)
{
	struct imt_i2c *i2c = platform_get_drvdata(pdev);

	i2c_del_adapter(&i2c->adap);

	return 0;
}

static struct of_device_id imt_i2c_device_ids[] = {
	{ .compatible = "imt,i2c", },
	{},
};
MODULE_DEVICE_TABLE(of, imt_i2c_device_ids);

static struct platform_driver imt_i2c_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "imt-i2c",
		.of_match_table = imt_i2c_device_ids,
	},
	.probe		= imt_i2c_probe,
	.remove		= imt_i2c_remove,
};
module_platform_driver(imt_i2c_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@babayev.com>");
MODULE_DESCRIPTION("IMT I2C driver");
MODULE_LICENSE("GPL");
