// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2018 Ruslan Babayev.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/mux/driver.h>

struct imt_mux {
	void __iomem *reg;
};

static int imt_mux_set(struct mux_control *mux, int state)
{
	struct imt_mux *priv = mux_chip_priv(mux->chip);

	iowrite8(state, priv->reg);
	return 0;
}

static const struct mux_control_ops imt_mux_ops = {
	.set	= imt_mux_set,
};

static int imt_mux_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct resource *res;
	struct mux_chip *mux_chip;
	struct mux_control *mux;
	struct imt_mux *priv;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
		return -ENODEV;

	mux_chip = devm_mux_chip_alloc(dev, 1, sizeof (*priv));
	if (IS_ERR(mux_chip))
		return PTR_ERR(mux_chip);

	priv = mux_chip_priv(mux_chip);
	priv->reg = devm_ioremap_resource(dev, res);
	if (IS_ERR(priv->reg))
		return PTR_ERR(priv->reg);

	mux_chip->ops = &imt_mux_ops;
	mux = &mux_chip->mux[0];
	mux->states = 32;
	mux->idle_state = MUX_IDLE_AS_IS;

	return devm_mux_chip_register(dev, mux_chip);
}

static int imt_mux_remove(struct platform_device *pdev)
{
	return 0;
}

static struct of_device_id imt_mux_device_ids[] = {
	{ .compatible = "imt,mux", },
	{},
};
MODULE_DEVICE_TABLE(of, imt_mux_device_ids);

static struct platform_driver imt_mux_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "imt-mux",
		.of_match_table = imt_mux_device_ids,
	},
	.probe		= imt_mux_probe,
	.remove		= imt_mux_remove,
};
module_platform_driver(imt_mux_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@babayev.com>");
MODULE_DESCRIPTION("IMT MUX controller driver");
MODULE_LICENSE("GPL");
