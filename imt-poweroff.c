// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2018 Ruslan Babayev.
 */

#include <linux/module.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/platform_device.h>

static void __iomem *reg;

static void imt_poweroff(void)
{
	u8 val;

	val = ioread8(reg);
	val &= ~3;
	iowrite8(val, reg);

	mdelay(1000);

	pr_emerg("unable to poweroff system\n");
}

static int imt_poweroff_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct resource *res;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
		return -ENODEV;

	reg = devm_ioremap_resource(dev, res);
	if (IS_ERR(reg))
		return PTR_ERR(reg);

	if (pm_power_off) {
		dev_err(dev, "pw_power_off already claimed");
		return -EBUSY;
	}

	pm_power_off = imt_poweroff;

	return 0;
}

static int imt_poweroff_remove(struct platform_device *pdev)
{
	if (pm_power_off == imt_poweroff)
		pm_power_off = NULL;

	return 0;
}

static const struct of_device_id imt_poweroff_device_ids[] = {
	{ .compatible = "imt,poweroff" },
	{}
};
MODULE_DEVICE_TABLE(of, imt_poweroff_device_ids);

static struct platform_driver imt_poweroff_driver = {
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= "imt-poweroff",
		.of_match_table = imt_poweroff_device_ids,
	},
	.probe	= imt_poweroff_probe,
	.remove	= imt_poweroff_remove,
};
module_platform_driver(imt_poweroff_driver);

MODULE_AUTHOR("Ruslan Babayev <ruslan@babayev.com>");
MODULE_DESCRIPTION("IMT poweroff driver");
MODULE_LICENSE("GPL");
